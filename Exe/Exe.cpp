#include "Exe.h"

#include <iostream>

// Include windows for dynamic dll loading
#include <Windows.h>


int getExeInt() {
    return 123;
}

// ================================
// Implementation of System methods
// ================================
System::System() : x_(42) {
}

void System::setX(const int x) {
    x_ = x;
}

int System::getX() const {
    return x_;
}

void System::printX() const {
    std::cout << "x: " << x_ << std::endl;
}


// ====
// Main
// ====

using GetIntFunc_t = int(*)();
using InitFunc_t = void(*)(ExeIntFunc_t, System*);

// Global system variable used to pass to the dll
System sys;

int main() {
    std::cout << "Main running" << std::endl;

    // Dynamically load the dll
    const auto hDll = LoadLibrary(L"TestDll");

    // Get the main init function from the dll
    const auto initFunc = GetProcAddress(hDll, "doInit");
    const auto initDll = reinterpret_cast<InitFunc_t>(initFunc);

    // Pass our function pointer and object pointer to the dll which can then use them.
    initDll(getExeInt, &sys);

    // Get another function to test that the dll can work with given pointers
    const auto func = GetProcAddress(hDll, "getInt");
    const auto getIntDll = reinterpret_cast<GetIntFunc_t>(func);

    // Value retured from the dll and value stored in the system should match
    // since the dll changes the value in system when getIntDll is called.
    std::cout << "Int val from dll: " << getIntDll() << std::endl;
    std::cout << "Int val from sys: " << sys.getX() << std::endl;

    // Exit and clean
    std::cout << "Exe end" << std::endl;
    FreeLibrary(hDll);
    return 0;
}