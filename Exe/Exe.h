#pragma once

/**
 * Typedef of getExeInt pointer to be used both from the dll and from the exe.
 */
using ExeIntFunc_t = int(*)();

/**
 * Function returning int used to demonstrate how to pass function pointers to the dll.
 */
int getExeInt();


/**
* This is a base class usable both from exe and dll.
* There should be an implementation of this class in the exe and a pointer to it
* is passed to the dll in the init fuction. This way, the dll can use things from the exe
* dynamically.
* This class does not have to be abstract, but all functions called from the dll must be virtual
* so the linker does not try to find them during the link time. In our case it is both setX and
* getX. Since we do not use printX in the Dll it does not have to be virtual.
* 
* It would be probably best to have an abstract base class where all the usable functions would be
* defined. E.g., ISystem with abstract setX and getX functions.
*/
class System {
    int x_;
public:
    System();
    virtual ~System() = default;

    virtual void setX(int x);
    virtual int getX() const;
    void printX() const;
};
