# Exe to dll handshake

Example project showing how to dynamically get functions from a dll and how to use these functions
to pass function pointers and object pointers back to the dll, effectively being able to use the exe's objects from the dll.

## Getting started

The repository contains a single Visual studio 2017 solution with two projects, one builds to the exe
and the other one builds to the dll. Windows is required since we include `<windows.h>` to be able to `LoadLibrary()` etc.

## The problem

When reading about idTech4 engine, I really liked the idea of having an engine as an `exe` and a game as a `dll` because then,
I can simply run different games on the same engine by just swapping the dll and I wanted to use the same technique in my project
[Platformer Engine](https://bitbucket.org/smejkapa/platformerengine).

Loading of the dll is relatively easy and since the game interface should be generic, the engine cannot expect much from it. The engine
just has to be able to create the game and update it somehow.

However, the game needs to use the engine extensively; therefore, it would make more sense to create the game as `exe` and engine as `dll`.
But that would defeat the purpose of the modularity described earlier. So the question was - how to pass object pointers from the exe to the dll.

## The process

The process is embarrassingly simple when one knows what to do. The key is to create a handshake between the exe and the dll.

1. The exe loads the dll as usual and takes a `doInit` function from it.
1. The exe calls the `doInit` function, and fills in required pointers to functions and objects of the exe.
1. In the `doInit` function, the dll saves the provided pointers. The dll now has access to the exe's objects.

Important notes are

* All methods in the class we pass from the exe to the dll we call in the dll have to be virtual otherwise, the linker will try to find the implementation and will fail.
* Other than virtual methods may be present in the class, as long as they are not called from the dll.
* It is advisable to make a base abstract class and pass a pointer to it.

The main inspiration comes from the idTech3 and idTech4 engines. The idTech3 is written in C, so only function pointers are passed there. In the idTech4
C++ was used, and pointers to systems are transferred. However, it seems that the system is no longer used by id, since in the DOOM3-BFG edition of idTech4
the game builds to a `lib` instead of a `dll` and is then linked to a single `exe` used to run the game.

## Resources

* [idTech3 engine - Quake 2](https://github.com/id-Software/Quake-2)
* [idTech4 engine - Doom 3](https://github.com/id-Software/DOOM-3)
* [idTech4 engine - Doom 3 BFG](https://github.com/id-Software/DOOM-3-BFG)
* [Fabien Sanglard's blog](http://fabiensanglard.net/) ([Quake 2](http://fabiensanglard.net/quake2/quake2Polymorphism.php), [Doom 3](http://fabiensanglard.net/doom3/index.php))

