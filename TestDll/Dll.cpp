#include "Dll.h"

ExeIntFunc_t exeIntFunc;
System* system;

int getInt() {
    // Do some calculation and store the result to system
    // to check both get and set functions work
    const auto ret = exeIntFunc() + system->getX();
    system->setX(ret);
    return ret;
}

void doInit(const ExeIntFunc_t exeFunc, System* exeSystem) {
    // Just store the pointers for later use
    exeIntFunc = exeFunc;
    system = exeSystem;
}
