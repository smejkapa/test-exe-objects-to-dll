#pragma once

// Include the common header, only declarations should be in it
#include "../Exe/Exe.h"

extern "C" {
    /** 
     * Function to be called from the exe which changes values in given System and 
     * returns int calculated from these values.
     */
    __declspec(dllexport) int getInt();

    /**
     * Function to be called at the beginning of the communication with the dll.
     * Demonstrates how to exchange function pointers and object pointers to be used
     * from the dll.
     */
    __declspec(dllexport) void doInit(ExeIntFunc_t, System*);
}